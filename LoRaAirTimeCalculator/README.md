# LoRaAirTimeCalc.py - Python 3/MicroPython script to calculate air time in LoRa

This simple **Python 3/MicroPython script calculates the data rate (in bps) and air time (in seconds) of the [LoRa](https://lora-alliance.org/) packet**. 

This software was first annoucement in [Air Time calculation in (Micro)Python topic in Pycom Forum](https://forum.pycom.io/topic/3135/air-time-calculation-in-micro-python).

Links about LoRa data rate and air time calculations :

* [LoRa data rate calculator](http://www.rfwireless-world.com/calculators/LoRa-Data-Rate-Calculator.html);
* [LoRa air time calculator](https://www.loratools.nl/#/airtime);
* [Spreadsheet for LoRa airtime calculation](https://www.thethingsnetwork.org/forum/t/spreadsheet-for-lora-airtime-calculation/1190).

Remember that **LoRaWan adds 13 bytes as header**, e. g., a 14 bytes payload in LoRaWan = 27 bytes payload in LoRa.

## Available functions :

* `dataratetheoretical(sf, bw, cr)` : spreading factor (sf) and using Pycom constants for bandwidth (bw) and coding rate (cr), 
it calculates the DR (data rate) in bps;
* `airtimetheoretical(payloadsize, sf, bw, cr)` : payload size in bytes, spreading factor (sf) and using Pycom constants for 
bandwidth (bw) and coding rate (cr), it calculates the air time (in seconds) for LoRa-RAW/LoRa-MAC, where there is a default 
preamble of 8 bytes + >= 5 bytes of CRC, header, etc. Fist return value is total air time (of the LoRa packet), 2nd for the 
preamble, 3rd for the payload, 4th for the symbols, 5th is the number of symbols.

Pycom constants in 'LoRa.py' (not needed in Pycom MicroPython) :

```
BW_125KHZ = 0
BW_250KHZ = 1
BW_500KHZ = 2

CODING_4_5 = 1
CODING_4_6 = 2
CODING_4_7 = 3
CODING_4_8 = 4
```

## Examples :

Using softwares 'ipython'/'Jupyter Notebook' (for Python 3).

Data rate (in bps) for SF = 7, BW = 500 kHz, CR = 4/5 : 

```python
In [1]: from LoRaAirTimeCalc import *
In [2]: dataratetheoretical(7, LoRa.BW_500KHZ, LoRa.CODING_4_5)
Out[2]: 21875.0
```

Loop listing the spreading factor (sf = 7-12) versus data rate (in bps) for BW = 125 kHz, CR = 4/5 :

```python
In [1]: from LoRaAirTimeCalc import *
In [2]: [(sf, dataratetheoretical(sf, LoRa.BW_125KHZ, LoRa.CODING_4_5)) for sf in range(7, 13)]
Out[2]: 
[(7, 5468.75),
 (8, 3125.0),
 (9, 1757.8125),
 (10, 976.5625),
 (11, 537.109375),
 (12, 292.96875)]
```

Air time (in seconds) for payload = 20 bytes, SF = 10, BW = 125 kHz, CR = 4/5. 1st value is the packet air time :

```python
In [1]: from LoRaAirTimeCalc import *
In [2]: airtimetheoretical(20, 10, LoRa.BW_125KHZ, LoRa.CODING_4_5)
Out[2]: (0.37068799999999996, 0.100352, 0.27033599999999997, 0.008192, 33)
```

Loop listing the payload in bytes (1-32) versus the packet air time im ms (miliseconds) for SF = 8, BW = 125 kHz, CR = 4/5. The air time changes 
only after 4-5 bytes of payload :

```python
In [1]: from LoRaAirTimeCalc import *
In [2]:  [(payloadsize, airtimetheoretical(payloadsize, 8, LoRa.BW_125KHZ, LoRa.CODING_4_5)[0]*1000) for payloadsize in range(1, 33)]
Out[2]:
[(1, 51.711999999999996),
 (2, 51.711999999999996),
 (3, 61.952),
 (4, 61.952),
 (5, 61.952),
 (6, 61.952),
 (7, 72.19200000000001),
 (8, 72.19200000000001),
 (9, 72.19200000000001),
 (10, 72.19200000000001),
 (11, 82.432),
 (12, 82.432),
 (13, 82.432),
 (14, 82.432),
 (15, 92.672),
 (16, 92.672),
 (17, 92.672),
 (18, 92.672),
 (19, 102.91199999999999),
 (20, 102.91199999999999),
 (21, 102.91199999999999),
 (22, 102.91199999999999),
 (23, 113.15199999999999),
 (24, 113.15199999999999),
 (25, 113.15199999999999),
 (26, 113.15199999999999),
 (27, 123.392),
 (28, 123.392),
 (29, 123.392),
 (30, 123.392),
 (31, 133.632),
 (32, 133.632)
```